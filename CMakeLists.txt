cmake_minimum_required(VERSION 3.2)
project(io_thrasher)

set(CMAKE_CXX_STANDARD 14)

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
find_package(Boost REQUIRED COMPONENTS program_options)

add_executable(io_thrasher main.cpp)

target_link_libraries(io_thrasher
        Threads::Threads Boost::program_options)

target_include_directories(io_thrasher
        PRIVATE ${Boost_INCLUDE_DIRS})