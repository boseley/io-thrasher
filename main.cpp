#include <fcntl.h>
#include <unistd.h>

#include <thread>
#include <random>
#include <functional>
#include <iostream>

#include <boost/program_options.hpp>

namespace po = boost::program_options;

constexpr auto BLOCK_SIZE = 1024 * 1024;
constexpr auto BLOCK_COUNT = 10;

std::array<unsigned char, BLOCK_SIZE> write_block;
std::default_random_engine random_eng{std::random_device{}()};
std::uniform_int_distribution<> sleep_dist{100, 1000};

void thrash_file(std::string fn) {
    while (1) {
        unsigned char read_buffer[BLOCK_SIZE];

        int fd = ::open(fn.c_str(), O_CREAT | O_WRONLY | O_TRUNC | O_CLOEXEC | O_SYNC, S_IRUSR | S_IWUSR);
        if (fd == -1) throw std::runtime_error("generate_file can't open " + fn);

        //write the file
        for (auto i = 0; i < BLOCK_COUNT; i++)
            write(fd, write_block.data(), write_block.size());

        // tell linux to discard the buffer so its read from the device not the cache
        posix_fadvise(fd, 0, 0, POSIX_FADV_DONTNEED);
        close(fd);

        fd = open(fn.c_str(), O_RDONLY | O_CLOEXEC);
        if (fd == -1) throw std::runtime_error("read_file can't open " + fn);

        //read it back
        for (auto i = 0; i < BLOCK_COUNT; i++)
            read(fd, read_buffer, sizeof read_buffer);

        // tell linux to discard the buffer so its read from the device not the cache
        posix_fadvise(fd, 0, 0, POSIX_FADV_DONTNEED);
        close(fd);

        // add a small variable delay so the threads read/writes get out of sync
        std::this_thread::sleep_for(std::chrono::milliseconds{sleep_dist(random_eng)});
    }
}

int main(int argc, char *argv[]) {
    unsigned option_thread_count{2};
    unsigned char option_pattern{0xAA};

    try {
        po::options_description desc("Allowed options");
        desc.add_options()
                ("help", "produce help message")
                ("threads,t", po::value<unsigned>(&option_thread_count)->default_value(2),
                 "set number of threads to run")
                ("pattern,p", po::value<std::string>(),
                 "pattern in hex to write,  0x55"
                 "set number of threads to run");

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if (vm.count("help")) {
            std::cout << desc << "\n";
            return 0;
        }

        if (vm.count("pattern")) {
            option_pattern = 0xFFu & std::stoul(vm["pattern"].as<std::string>(), nullptr, 16);
        }
    }
    catch (std::exception &e) {
        std::cerr << "error: " << e.what() << "\n";
        return 1;
    }
    catch (...) {
        std::cerr << "Exception of unknown type!\n";
    }

    std::cout << "pattern : 0x" << std::hex << static_cast<unsigned int>(option_pattern) << std::endl;
    std::cout << "threads count : " << std::dec << option_thread_count << std::endl;

    write_block.fill(option_pattern);
    std::vector<std::thread> workers;
    for (unsigned i = 0; i < option_thread_count; i++)
        workers.emplace_back(
                [filename = "thrash_file_" + std::to_string(i) + ".txt"]() {
                    thrash_file(filename);
                }
        );

    for (auto &th : workers)
        th.join();

    return 0;
}